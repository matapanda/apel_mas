// See post: http://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/

var map = L.map( 'map', {
  center: [-7.98, 112.63],
  minZoom: 14,
  zoom: 3
})

L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ['a', 'b', 'c']
}).addTo( map )

var myURL = jQuery( 'script[src$="leaf-demo.js"]' ).attr( 'src' ).replace( 'leaf-demo.js', '' )

var myIcon = L.icon({
  iconUrl: myURL + 'images/location32.png',
  iconRetinaUrl: myURL + 'images/location64.png',
  iconSize: [40, 40],
  iconAnchor: [9, 21],
  popupAnchor: [10, -14]
})

for ( var i=0; i < markers.length; ++i )
{
 L.marker( [markers[i].lat, markers[i].lng], {icon: myIcon} )
  .bindPopup( '<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a>' )
  .addTo( map );
}
console.log("sip dong");

map.on('click', function (result) {
  // L.marker([result.x, result.y]).addTo(map) 
  // console.log("sip");
  console.log(result);
  // console.log(result.y);
});
