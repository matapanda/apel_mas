<!DOCTYPE html>
<!--
	ubusina by freshdesignweb.com
	Twitter: https://twitter.com/freshdesignweb
	URL: https://www.freshdesignweb.com
-->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>APEL MAS</title>
	<link rel="stylesheet" href="<?php echo base_url()?>template1/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>template1/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>template1/css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,800,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=BenchNine:300,400,700' rel='stylesheet' type='text/css'>
</head>
<body>
	
	<!-- ====================================================
	header section -->
	<header class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-5 header-logo">
					<br>
					 <table border="0" width="1200px">
       <tr>
        <th width="250px"><img src="<?php echo base_url()?>template1/img/logo.png" alt="" class="img-responsive logo"></a></th>
        <th width="700px"></th>
        <th width="250px"><img src="<?php echo base_url()?>template1/img/malangl.png" alt="" class="img-responsive logo"></a></th>
      </tr>
     </table>
				</div>

			

					  </div><!-- / .container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</header> <!-- end of header area -->

			
			

			<section class="slider" id="home">
				<div class="container-fluid">
					<div class="row">

					    <div id="carouselHacked" class="carousel slide carousel-fade" data-ride="carousel">
							<div class="header-backup"></div>
					        <!-- Wrapper for slides -->
					        <div class="carousel-inner" role="listbox">
					            <div class="item active">
					            	<img src="<?php echo base_url()?>template1/img/event1.jpg" alt="">
					                <div class="carousel-caption">
				               			
					                </div>
					            </div>
					            <div class="item">
					            	<img src="<?php echo base_url()?>template1/img/event2.jpg" alt="">
					                <div class="carousel-caption">
				               		
					                </div>
					            </div>
					            <div class="item">
					            	<img src="<?php echo base_url()?>template1/img/event3.jpg" alt="">
					                <div class="carousel-caption">
				               			
					                </div>
					            </div>
					            </div>
					        </div>

					        <!-- Controls -->
					        <a class="left carousel-control" href="#carouselHacked" role="button" data-slide="prev">
					            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					            <span class="sr-only">Previous</span>
					        </a>
					        <a class="right carousel-control" href="#carouselHacked" role="button" data-slide="next">
					            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					            <span class="sr-only">Next</span>
					        </a>
					    </div>

					</div>
				</div>
			</section><!-- end of slider section -->


			<!-- about section -->
			
			<section class="about text-center" id="about">
				<div class="container">
		
		 <style type="text/css">
              body {
	            color: #fff;
	            background: url('img/balaikota.jpg');
	            background-size: cover;     /* Tambahkan baris berikut */
	            background-attachment: fixed;
}
          </style>
					<div class="row">
						<h2>APEL MAS</h2>
						<h4>Aplikasi Pelayanan Masyarakat Kota Malang </h4>

					<div class="col-md-12 ">
                          <div class="about-details">
								
									<div class="row">
                    <?php

                        if($list_menu){
                            if($list_menu->msg_main->status){
                                $detail_data = $list_menu->msg_detail;

                                    $item_layanan = $detail_data->item;
                                    $base_url = $detail_data->url_core;

                                foreach ($item_layanan as $r_item_layanan => $v_item_layanan) {
                                    $kategori   = $v_item_layanan->kategori;
                                        $title_kategori = $kategori->nama_kategori;

                                    if(isset($v_item_layanan->list_menu)){
                                        $list_menu  = $v_item_layanan->list_menu;
                                        
                                        foreach ($list_menu as $r_list_menu => $v_list_menu) {
                                            $id_page = $v_list_menu->id_page;
                                            $nama_page = $v_list_menu->nama_page;
                                            $foto_page = $v_list_menu->foto_page;
                                            $next_page = $v_list_menu->next_page;                            
                    ?>
                    <!-- <style type="text/css">
              body {
                color: #fff;
                background-color: #38559f;
                /*background: url('img/balaikota.jpg');*/
                background-size: cover;     /* Tambahkan baris berikut */
                background-attachment: fixed;
}
          </style> -->

                                    <div class="col-md-3">
                                        <div class="card text-center">
                                            <button style="border: none; background: transparent;" onclick="next_page('<?php print_r($id_page);?>', '<?php print_r($next_page);?>')">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <img src="<?php print_r($base_url.$foto_page);?>" alt="homepage" style="width: 100px; height: 100px;"/>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-lg-12" style="height: 40px;">
                                                        <h4 class="card-title"><?php print_r($nama_page);?></h4>
                                                    </div>
                                                </div>                                
                                            </div>
                                            </button>
                                        </div>
                                    </div>
                                    
                    <?php
                                        }
                                    }
                                }
                            }
                        }
                        

                    ?>
                </div>
								</div>
							</div>
						</div>

						</div>
 
					</div>

				</div>
			</section><!-- end of about section -->


			

		
		
			<!-- footer starts here -->
			<footer class="footer clearfix">
				<div class="container">
					<div class="row">
						<div class="col-xs-6 footer-para">
							<p>&copy; <a href="https://www.freshdesignweb.com">freshDesignweb.com</a> All right reserved</p>
						</div>

						<div class="col-xs-6 text-right">
							<a href=""><i class="fa fa-facebook"></i></a>
							<a href=""><i class="fa fa-twitter"></i></a>
							<a href=""><i class="fa fa-skype"></i></a>
						</div>
					</div>
				</div>
			</footer>



	

	<!-- script tags
	============================================================= -->
	<script src="<?php echo base_url()?>template1/js/jquery-2.1.1.js"></script>
	<script src="<?php echo base_url()?>template1/http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url()?>template1/js/gmaps.js"></script>
	<script src="<?php echo base_url()?>template1/js/smoothscroll.js"></script>
	<script src="<?php echo base_url()?>template1/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>template1/js/custom.js"></script>
	
</body>
</html>