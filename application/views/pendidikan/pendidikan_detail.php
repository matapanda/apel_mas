<?php
	// print_r("<pre>");
	// print_r($detail_info);
	$jenis_sekolah = "";
	$kec_sekolah = "";
	$nama_sekolah = "";

	$web_sekolah = "";
	$alamat_sekolah = "";	
	$tlp_sekolah = "";
	

	$loc = "";

	if($detail_info){
		$msg_main = $detail_info->msg_main;
		if ($msg_main->status) {
			$msg_detail = $detail_info->msg_detail;
				$item 	= $msg_detail->item;
				// print_r($item);

				$jenis_sekolah = $item->nama_jenis;
				$kec_sekolah = $item->nama_kecamatan;
				$nama_sekolah = $item->nama_sekolah;

				$detail_sekolah = json_decode(str_replace("'", "\"", $item->detail_sekolah));

				// print_r($detail_sekolah);

				$web_sekolah 	= $detail_sekolah->url;
				$alamat_sekolah = $detail_sekolah->alamat;	
				$tlp_sekolah 	= $detail_sekolah->tlp;
				

				$loc = json_decode(str_replace("'", "\"", $item->lokasi));
					$lat = $loc[0];
					$long = $loc[1];
		}
	}
?>

			<div class="colorlib-contact">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
							<span class="heading-meta"><?php print_r($jenis_sekolah);?></span>
							<h2 class="colorlib-heading"><?php print_r($nama_sekolah);?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-globe-outline"></i>
								</div>
								<div class="colorlib-text">
									<p><a href="#"><?php print_r($web_sekolah);?></a></p>
								</div>
							</div>

							<div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-map"></i>
								</div>
								<div class="colorlib-text">
									<p><?php print_r($alamat_sekolah);?></p>
								</div>
							</div>

							<div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-phone"></i>
								</div>
								<div class="colorlib-text">
									<p><a href="tel://"><?php print_r($tlp_sekolah);?></a></p>
								</div>
							</div>

							<div id="colorlib-counter" class="colorlib-counters" style="background-color: transparent;" data-stellar-background-ratio="0.5">
								<div class="colorlib-narrow-content">
									<div class="row">
									</div>
									<div class="row">
										<div class="col-md-3 text-center animate-box">
											<span class="icon"><i class="flaticon-skyline"></i></span>
											<span class="colorlib-counter js-counter" data-from="0" data-to="194" data-speed="5000" data-refresh-interval="50"></span>
											<span class="colorlib-counter-label">jumlah SD Negeri</span>
										</div>
										<div class="col-md-2"></div>
										<div class="col-md-3 text-center animate-box">
											<span class="icon"><i class="flaticon-skyline"></i></span>
											<span class="colorlib-counter js-counter" data-from="0" data-to="139" data-speed="5000" data-refresh-interval="50"></span>
											<span class="colorlib-counter-label">Jumlah SD SWASTA</span>
										</div>
									</div>
								</div>
							</div>

						</div>

						<!-- maps detail -->
						<div class="col-md-8 col-md-push-1">
							<div class="row">
								<div class="col-md-10 col-md-offset-1 col-md-pull-1 animate-box" data-animate-effect="fadeInLeft">

									<!-- maps -->
									<div class="form-group">
										<div id="map" style="width: 100%; height: 700px;">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	<script type="text/javascript">
    	// See post: http://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/
    	
		var map = L.map( 'map', {
		  center: [<?php print_r($lat);?>, <?php print_r($long);?>],
		  minZoom: 14,
		  zoom: 3
		})

		L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
		  subdomains: ['a', 'b', 'c']
		}).addTo( map )
		
		var myIcon = L.icon({
		  
		  iconUrl: '<?php print_r($url_icon.$icon_32);?>',
		  iconRetinaUrl: '<?php print_r($url_icon.$icon_64);?>',
		  
		  iconSize: [40, 40],
		  iconAnchor: [9, 21],
		  popupAnchor: [10, -14]
		})


			// console.log(markers[i]);
		 L.marker( [<?php print_r($lat);?>, <?php print_r($long);?>], {icon: myIcon} )
		  .bindPopup( '<a href="#"><?php print_r($nama_sekolah);?></a>' )
		  .addTo( map );
		
		// console.log("sip dong");

		map.on('click', function (result) {
		  // L.marker([result.x, result.y]).addTo(map) 
		  // console.log("sip");
		  console.log(result);
		  // console.log(result.y);
		});

    </script>