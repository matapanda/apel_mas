			<div class="colorlib-about">
				<div class="colorlib-narrow-content">
					<div class="row row-bottom-padded-md">
						<div class="col-md-12">
							<div class="about-img animate-box" data-animate-effect="fadeInLeft" style="width: 100%; height: 1000;">
								 <h1 class="colorlib-heading">INFO PENDIDIKAN di KOTA MALANG</h1> 
								 <div id="map"></div>
    
							</div>
						</div>
						<div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
				
						</div>
					</div>
				</div>
			</div>

			


<!-- maps -->
	<!-- <script type='text/javascript' src='<?php echo base_url()?>Leaflet_Example-master/maps/markers.json'></script> -->
    <!-- <script type='text/javascript' src='<?php echo base_url()?>Leaflet_Example-master/maps/leaf-demo.js'></script> -->
    <script type="text/javascript">
    	// See post: http://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/
    	var array_marker = JSON.parse('<?php print_r($array_marker);?>');
    	
		var map = L.map( 'map', {
		  center: [-7.98, 112.63],
		  minZoom: 10,
		  zoom: 2
		})

		L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
		  subdomains: ['a', 'b', 'c']
		}).addTo( map )
		
		var myIcon = L.icon({
		  
		  iconUrl: '<?php print_r($url_icon.$icon_32);?>',
		  iconRetinaUrl: '<?php print_r($url_icon.$icon_64);?>',
		  
		  iconSize: [40, 40],
		  iconAnchor: [9, 21],
		  popupAnchor: [10, -14]
		})

		markers = array_marker;

		for ( var i=0; i < markers.length; ++i )
		{

			// console.log(markers[i]);
		 L.marker( [markers[i].lat, markers[i].lng], {icon: myIcon} )
		  .bindPopup( '<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a>' )
		  .addTo( map );
		}
		// console.log("sip dong");

		map.on('click', function (result) {
		  // L.marker([result.x, result.y]).addTo(map) 
		  // console.log("sip");
		  console.log(result);
		  // console.log(result.y);
		});

    </script>