<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SD-Kec</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href="<?php echo base_url()?>assets/template/template5/https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/bootstrap.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/flexslider.css">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/fonts/flaticon/font/flaticon.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/owl.theme.default.min.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url()?>assets/template/template5/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="border js-fullheight">
			<h1 id="colorlib-logo"><a href="index.html"><img src="<?php echo base_url()?>template/template1/img/logo.png" alt="" class="img-responsive logo"></a></h1>
			<nav id="colorlib-main-menu" role="navigation">
				<ul>
					
					<!-- <li><a href="index.html">Home</a></li>
					<li><a href="work.html">Project</a></li> -->
					<li><a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_jenis">HALAMAN UTAMA</a></li>
					<li class="colorlib-active"><a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_v_kecamatan">KECAMATAN KLOJEN</a></li>
					<li><a href="services.html">KECAMATAN BLIMBING</a></li>
					<li><a href="services.html">KECAMATAN KEDUNGKANDANG</a></li>
					<li><a href="services.html">KECAMATAN LOWOKWARU</a></li>
					<li><a href="services.html">KECAMATAN SUKUN</a></li>
			<!-- 		<li><a href="blog.html">Blog</a></li>
					<li><a href="contact.html">Contact</a></li> -->
				</ul>
			</nav>

			<div class="colorlib-footer">
				<p><small>&copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Made with <i class="icon-heart" aria-hidden="true"></i> by <a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_jenis" target="_blank">NCC SQUAD</a></p>
				<ul>
					<li><a href="#"><i class="icon-facebook2"></i></a></li>
					<li><a href="#"><i class="icon-twitter2"></i></a></li>
					<li><a href="#"><i class="icon-instagram"></i></a></li>
					<li><a href="#"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>

		</aside>


		<div id="colorlib-main">

			<div class="colorlib-blog">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
							<span class="heading-meta">SEKOLAH DASAR NEGERI DAN SWASTA</span>
							<h2 class="colorlib-heading">KECAMATAN KLOJEN - KOTA MALANG</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="blog-entry">
								<a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_v_kecamatan" class="blog-img"><img src="<?php echo base_url()?>assets/template/template5/images/blog-1.jpg" class="img-responsive" alt="HTML5 Bootstrap Template by colorlib.com"></a>
								<div class="desc">
									<span><small>2019 </small> | <small> Sekolah Dasar Kecamatan Klojen </small> </span>
									<h3><a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_detail">SDN KLOJEN</a></h3>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="blog-entry">
								<a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_v_kecamatan" class="blog-img"><img src="<?php echo base_url()?>assets/template/template5/images/blog-2.jpg" class="img-responsive" alt="HTML5 Bootstrap Template by colorlib.com"></a>
								<div class="desc">
									<span><small>2019 </small> | <small> Sekolah Dasar Kecamatan Klojen </small></span>
									<h3><a href="blog.html">SDN KIDULDALEM 1</a></h3>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="blog-entry">
								<a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_v_kecamatan" class="blog-img"><img src="<?php echo base_url()?>assets/template/template5/images/blog-3.jpg" class="img-responsive" alt="HTML5 Bootstrap Template by colorlib.com"></a>
								<div class="desc">
									<span><small>2019 </small> | <small> Sekolah Dasar Kecamatan Klojen </small></span>
									<h3><a href="blog.html">SDN KIDULDALEM 2</a></h3>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="blog-entry">
								<a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_v_kecamatan" class="blog-img"><img src="<?php echo base_url()?>assets/template/template5/images/blog-4.jpg" class="img-responsive" alt="HTML5 Bootstrap Template by colorlib.com"></a>
								<div class="desc">
									<span><small>2019 </small> | <small> Sekolah Dasar Kecamatan Klojen </small>></span>
									<h3><a href="blog.html">SDN KAUMAN 1</a></h3>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="blog-entry">
								<a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_v_kecamatan" class="blog-img"><img src="<?php echo base_url()?>assets/template/template5/images/blog-5.jpg" class="img-responsive" alt="HTML5 Bootstrap Template by colorlib.com"></a>
								<div class="desc">
									<span><small>2019 </small> | <small> Sekolah Dasar Kecamatan Klojen </small> </span>
									<h3><a href="blog.html">SDN KAUMAN 2</a></h3>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="blog-entry">
								<a href="<?php echo base_url()?>front_data/Pendidikanpage/pendidikan_v_kecamatan" class="blog-img"><img src="<?php echo base_url()?>assets/template/template5/images/blog-6.jpg" class="img-responsive" alt="HTML5 Bootstrap Template by colorlib.com"></a>
								<div class="desc">
									<span><small>2019 </small> | <small> Sekolah Dasar Kecamatan Klojen </small></span>
									<h3><a href="blog.html">SDN KAUMAN 3</a></h3>
								</div>
							</div>
						</div>
					</div>

					<!-- <div class="row">
						<div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
							<ul class="pagination">
								<li class="disabled"><a href="#">&laquo;</a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">&raquo;</a></li>
							</ul>
						</div>
					</div> -->
				</div>
			</div>

		
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets/template/template5/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.flexslider-min.js"></script>
	<!-- Sticky Kit -->
	<script src="<?php echo base_url()?>assets/template/template5/js/sticky-kit.min.js"></script>
	<!-- Owl carousel -->
	<script src="<?php echo base_url()?>assets/template/template5/js/owl.carousel.min.js"></script>
	<!-- Counters -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.countTo.js"></script>
	
	
	<!-- MAIN JS -->
	<script src="<?php echo base_url()?>assets/template/template5/js/main.js"></script>

	</body>
</html>

