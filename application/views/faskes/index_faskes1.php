<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Maps</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/bootstrap.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/flexslider.css">
    <!-- Flaticons  -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/fonts/flaticon/font/flaticon.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/template/template5/css/style.css">

    <link rel="stylesheet" type="text/css" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />

    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js'></script>
    <script type='text/javascript' src='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'></script>

    <!-- Modernizr JS -->
    <script src="<?php echo base_url()?>assets/template/template5/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
    <div id="colorlib-page">
        <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
        <aside id="colorlib-aside" role="complementary" class="border js-fullheight">
            <h1 id="colorlib-logo"><a href="index.html"><img src="<?php echo base_url()?>assets/template/template1/img/logo.png" alt="" class="img-responsive logo"></a></h1>
           <!--  <div class="fancy-collapse-panel">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default"> -->
<center><form>
<input class="MyButton1" type="button" value="HOME" onclick="window.location.href='http://localhost/f_kios/beranda/layanan'" />
</form></center>
            <!-- <nav id="colorlib-main-menu" role="navigation">
            <ul> <li><a href="http://localhost/f_kios/beranda/faskes/2899dcb1416a59041a5a337b84901a2a613e08cd71f5cb47939dd773e80a2c90"><i class="fa fa-home"></i> <em>Home</em></a></li>
            </ul>

         </nav> -->
                <nav id="colorlib-main-menu" role="navigation">
                
                                        <?php
                                            if($list_menu){
                                                // print_r("<pre>");
                                                // print_r($list_menu);

                                                $array_marker = array();
                                                $main_sts = $list_menu->msg_main->status;
                                                $main_msg = $list_menu->msg_main->msg;

                                                $detail = $list_menu->msg_detail;
                                                    $item = $detail->item;

                                                $url_icon = $detail->url_core;

                                                // print_r($url_icon);

                                                $no_pagin = 1;
                                                $no_marker = 0;
                                                foreach ($item as $key => $value) {
                                        ?>
                                                 <!--    <div class="panel-heading" role="tab" id="heading_<?=$no_pagin?>">
                                                        <h4 class="panel-title"> -->

                                                          <ul> <li><a class="collapsed" href="<?php print_r(base_url()."faskes/detail/".hash("sha256",$value->id_jenis)."/".hash("sha256",$value->id_faskes));?>"><?=$value->nama_faskes?></a>   </li> </ul>
                                                    <!--     </h4>
                                                    </div> -->
   
        
                                        <?php
                                                    $latlng = json_decode(str_replace("'", "\"", $value->lokasi));
                                                    
                                                    $array_marker[$no_marker]["name"] = $value->nama_faskes;
                                                    $array_marker[$no_marker]["lat"] = $latlng[0];
                                                    $array_marker[$no_marker]["lng"] = $latlng[1];

                                                    $icon_32 = $value->icon_32;
                                                    $icon_64 = $value->icon_64;

                                                    $no_marker++;
                                                    $no_pagin++;
                                                    
                                                }   
                                            }

                                            $array_marker = json_encode($array_marker);

                                        ?>
                                    
                                 
                                </nav>
                                <!--  <nav id="colorlib-main-menu" role="navigation">
            <ul> <li><a href="http://localhost/f_kios/beranda/layanan"><i class="fa fa-home"></i> <em> <font size"100px">Beranda</font></em></a></li>
            </ul>
         </nav> -->
         <!-- <center> 
            <form>
                <input class="MyButton" type="button" value="<< Back" onclick="window.location.href='http://localhost/f_kios/beranda/layanan'" />
            </form>
        </center>
 -->
                                        
     </div>

                            
		</aside>

		<div id="colorlib-main">

			<!-- isi -->

            <?php
                if(isset($page)){
                    switch ($page) {
                        case 'faskes_home': include "faskes_home.php"; 
                            break;

                        case 'faskes_detail': include "faskes_detail.php"; 
                            break;
                        
                        default:
                            break;
                    }
                }
            ?>

			<br><br><br><!-- <br><br><br><br> -->
			<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(<?php echo base_url()?>assets/template/template5/images/malang4.jpg);" data-stellar-background-ratio="0.5">
				<div class="overlay"></div>

				<div class="colorlib-narrow-content">
					<div class="row">
					</div>
					<div class="row">
						<div class="col-md-3 text-center animate-box">
							<span class="icon"><i class="flaticon-skyline"></i></span>
							<span class="colorlib-counter js-counter" data-from="0" data-to="34" data-speed="5000" data-refresh-interval="50"></span>
							<span class="colorlib-counter-label">jumlah </span>
						</div>
						<!-- <div class="col-md-3 text-center animate-box">
							<span class="icon"><i class="flaticon-skyline"></i></span>
							<span class="colorlib-counter js-counter" data-from="0" data-to="3653" data-speed="5000" data-refresh-interval="50"></span>
							<span class="colorlib-counter-label">Jumlah Sekolah SWASTA</span>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
<style>
input.MyButton:hover {
width: 120px;
padding: 10px;
cursor: pointer;
font-weight: bold;
font-size: 100%;
background: #FFC300;
color: #fff;
border: 1px solid #FFC300;
border-radius: 10px;
}
input.MyButton{
color: #FFC300;
background: #2C2C2C;
border: 1px solid #fff;
}
</style>

<style>
input.MyButton1 {
width: 120px;
padding: 10px;
cursor: pointer;
font-weight: bold;
font-size: 100%;
background:  #2C2C2C;
color: #fff;
border: 1px solid  #2C2C2C;
border-radius: 10px;
}
input.MyButton1:hover{
background:  #2C2C2C;
color:   #FFC300;
border: 1px solid  #FFC300;
}
</style>

	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets/template/template5/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.flexslider-min.js"></script>
	<!-- Sticky Kit -->
	<script src="<?php echo base_url()?>assets/template/template5/js/sticky-kit.min.js"></script>
	<!-- Owl carousel -->
	<script src="<?php echo base_url()?>assets/template/template5/js/owl.carousel.min.js"></script>
	<!-- Counters -->
	<script src="<?php echo base_url()?>assets/template/template5/js/jquery.countTo.js"></script>

	<!-- MAIN JS -->
	<script src="<?php echo base_url()?>assets/template/template5/js/main.js"></script>

	</body>
</html>